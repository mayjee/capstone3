import { useState, useContext } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Checkout({
  productId,
  name,
  description,
  price,
}) {
  const { user } = useContext(UserContext);
  const [quantity, setQuantity] = useState(1);
  const [showCheckout, setShowCheckout] = useState(false);

  const openCheckout = () => {
    // You can open the modal here if needed
    setShowCheckout(true);
  };

  const closeCheckout = () => {
    setShowCheckout(false);
  };

  const directCheckout = (e) => {
  e.preventDefault();

  fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    },
    body: JSON.stringify({
      userId: user.id,
      productId: productId,
      productName: name,
      quantity: quantity,
      price: price
    })
  })
  .then(response => response.json())
  .then(result => {
    if (result) {
      Swal.fire({
        title: 'Success!',
        icon: 'success',
        text: 'Your order has been placed successfully.'
      });
      closeCheckout(); 
      console.log(result);
    } else {
      Swal.fire({
        title: 'Error!',
        icon: 'error',
        text: 'Something went wrong. Please try again.'
      });
      closeCheckout(); 
    }
  });
};


  return (
    <>
      <Button variant="success" size="sm" onClick={openCheckout}>
        Buy Now
      </Button>

      <Modal show={showCheckout} onHide={closeCheckout}>
        <Form onSubmit={directCheckout}>
          <Modal.Header closeButton>
            <Modal.Title>Checkout</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group controlId="productName">
              <Form.Label>Product: {name}</Form.Label>
            </Form.Group>

            <Form.Group controlId="productDescription">
              <Form.Label>Description: {description}</Form.Label>
            </Form.Group>

            <Form.Group controlId="coursePrice">
              <Form.Label>Price: $ {price}</Form.Label>
            </Form.Group>

            <Form.Group controlId="productQuantity">
              <Form.Label>Quantity:</Form.Label>
              <Form.Control
                type="number"
                required
                min={1}
                value={quantity}
                onChange={(e) => {
                  const inputValue = parseInt(e.target.value, 10);

                  if (!isNaN(inputValue) && inputValue >= 0) {
                    setQuantity(inputValue);
                  }
                }}
              />
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            <div className="footer-left">
              <Form.Group controlId="courseTotal">
                <Form.Label>Total: $ {price * quantity}</Form.Label>
              </Form.Group>
            </div>

            <div>
              <Button variant="secondary footer-link" onClick={closeCheckout}>
                Close
              </Button>
              {user.id !== null ? (
                <Button variant="success" type="submit">
                  Place Order
                </Button>
              ) : (
                <Link className="btn btn-danger" to="/login">
                  Login
                </Link>
              )}
            </div>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}

