import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights() {

	return(
		<Row className="my-3">

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title className="py-2">
				            <h2>Handcrafted</h2>
				        </Card.Title>
				        <Card.Text>
				            Welcome to our world of exquisite violins, where every instrument is a masterpiece of craftsmanship. Each violin in our collection is meticulously handcrafted by skilled artisans who have dedicated their lives to the art of violin making. These craftsmen pour their passion, expertise, and attention to detail into every aspect of the violin's construction, from selecting the finest woods to carving and shaping each component with precision. The result is a violin that not only looks stunning but also feels like a work of art in your hands. When you choose one of our handcrafted violins, you're not just buying an instrument; you're acquiring a piece of musical history that resonates with the spirit of centuries-old tradition.
				        </Card.Text>
				    </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title className="py-2">
				            <h2>High-Quality Materials</h2>
				        </Card.Title>
				        <Card.Text>
				            At the heart of every great violin is the choice of materials, and we take this aspect seriously. We source only the highest quality woods, carefully selected for their tonal properties and visual appeal. The tops are crafted from spruce, known for its exceptional resonance, while the backs and sides are made from well-aged maple, providing stability and a warm, rich tone. Our commitment to quality extends beyond the wood itself; we use premium varnishes and fittings to ensure that each violin not only looks stunning but also ages gracefully, enhancing its sound and beauty over time. When you invest in one of our violins, you're investing in an instrument that will captivate you with its aesthetics and enchant you with its sound for generations to come.
				        </Card.Text>
				    </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title className="py-2">
				            <h2>Excellent Sound</h2>
				        </Card.Title>
				        <Card.Text>
				            The true test of a violin's quality lies in the sound it produces, and we are proud to say that our instruments excel in this regard. Every violin in our collection undergoes rigorous testing and fine-tuning to ensure it meets the highest standards of tonal excellence. Our commitment to producing high-quality sound means that whether you're a seasoned professional or a beginner taking your first steps in the world of music, you can trust that our violins will inspire you with their rich, resonant tones. From the delicate sweetness of the high notes to the deep, soulful resonance of the lower strings, each violin is crafted to deliver a broad range of emotions and musical expressions. Discover the joy of playing an instrument that allows you to create music that truly speaks to the heart.
				        </Card.Text>
				    </Card.Body>
				</Card>
			</Col>
		</Row>

	)
}