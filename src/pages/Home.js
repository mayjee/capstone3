import Banner from '../components/Banner.js';
import FeaturedProducts from '../components/FeaturedProducts.js';
import Highlights from '../components/Highlights.js';

export default function Home(){

	const data = {
	    title: "Violin Town",
	    content: "Handcrafted high quality violins",
	    destination: "/products",
	    label: "Explore"
	}

	return(
		<>
			<Banner data={data}/>
			<FeaturedProducts />
			<Highlights/>
		</>
	)
}