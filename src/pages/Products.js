import { useEffect, useState, useContext } from 'react';
// import ProductCard from '../components/ProductCard.js';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';

export default function Products(){

	const {user} = useContext(UserContext);

	
	const [products, setProducts] = useState([]);


	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(response => response.json())
		.then(data => {
			console.log(data);

			setProducts(data)
		})
	};

	useEffect(() => {
		fetchData();
	}, []);


	console.log(user.isAdmin)

	return(
		<>
			{
				(user.isAdmin === true) ?
					<AdminView productsData={products} fetchData={fetchData} />
					:
					<UserView productsData={products} />
			}
		</>
	)
}
