import { useState, useEffect, useContext } from 'react';
import { Button, Container, Card, Row, Col } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
// import Swal from 'sweetalert2';
import Checkout from '../components/Checkout';
import UserContext from '../UserContext';




export default function ProductView({ productsData }) {
  const { user } = useContext(UserContext);
  const { productId } = useParams();
  

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [showCheckout, setShowCheckout] = useState(false); // State to control the Checkout modal



  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((response) => response.json())
      .then((data) => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, [productId]);




  const openCheckout = () => {
    
    setShowCheckout(true);
  };



  const closeCheckout = () => {
    
    setShowCheckout(false);
  };



  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>$ {price}</Card.Text>

              <div className="text-center">
                {user.id !== null ? (
                  <>
                    <Button
                      className="btn btn-primary d-block"
                      onClick={openCheckout}
                    >
                      Buy Now
                    </Button>
                    {showCheckout && (
                      <Checkout
                        productId={productId}
                        name={name}
                        description={description}
                        price={price}
                        closeCheckout={closeCheckout}
                      />
                    )}
                  </>
                ) : (
                  <Link className="btn btn-danger" to="/login">
                    Login to Purchase
                  </Link>
                )}
              </div>

            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
